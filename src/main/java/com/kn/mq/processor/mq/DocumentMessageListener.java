package com.kn.mq.processor.mq;

import com.kn.mq.processor.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

@Component
public class DocumentMessageListener {

    private final DocumentService documentService;
    private final DocumentMessageSender documentMessageSender;

    @Autowired
    public DocumentMessageListener(DocumentService documentService, DocumentMessageSender documentMessageSender) {
        this.documentService = documentService;
        this.documentMessageSender = documentMessageSender;
    }

    @JmsListener(destination = "source", containerFactory = "jmsListenerContainerFactory")
    public void receiveMessage(Document document) {
        documentService.process(document);
        documentMessageSender.send(document);
    }
}
