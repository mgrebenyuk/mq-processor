package com.kn.mq.processor.mq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

@Component
public class DocumentMessageSender {

    @Autowired
    private final JmsTemplate jmsTemplate;

    public DocumentMessageSender(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    void send(Document document) {
        jmsTemplate.convertAndSend("destination", document);
    }
}
