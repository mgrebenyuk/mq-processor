package com.kn.mq.processor.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;

@Component
public class DocumentToStringConverter implements Converter<Document, String> {

    private final TransformerFactory transformerFactory;

    @Autowired
    public DocumentToStringConverter(TransformerFactory transformerFactory) {
        this.transformerFactory = transformerFactory;
    }

    @Override
    public String convert(Document source) {
        try {
            StringWriter writer = new StringWriter();
            transformerFactory.newTransformer()
                    .transform(new DOMSource(source), new StreamResult(writer));
            return writer.getBuffer().toString();
        } catch (Exception e) {
            throw new RuntimeException("Exception during Document to string conversion", e);
        }
    }
}
