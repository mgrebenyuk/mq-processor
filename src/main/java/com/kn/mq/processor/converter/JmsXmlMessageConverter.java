package com.kn.mq.processor.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

@Component
public class JmsXmlMessageConverter implements MessageConverter {

    private final DocumentToStringConverter documentToStringConverter;
    private final StringToDocumentConverter stringToDocumentConverter;

    @Autowired
    public JmsXmlMessageConverter(
            DocumentToStringConverter documentToStringConverter,
            StringToDocumentConverter stringToDocumentConverter) {
        this.documentToStringConverter = documentToStringConverter;
        this.stringToDocumentConverter = stringToDocumentConverter;
    }


    @Override
    public Message toMessage(Object object, Session session)
            throws MessageConversionException {
        try {
            return session.createTextMessage(documentToStringConverter.convert((Document) object));
        } catch (Exception e) {
            throw new MessageConversionException("Exception during converting to Message", e);
        }
    }

    @Override
    public Object fromMessage(Message message) throws JMSException, MessageConversionException {
        if (message instanceof TextMessage) {
            return convertFromTextMessage((TextMessage) message);
        }
        throw new MessageConversionException("Unsupported message type");
    }

    private Document convertFromTextMessage(TextMessage message) throws JMSException {
        return stringToDocumentConverter.convert(message.getText());
    }

}
