package com.kn.mq.processor.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import java.io.StringReader;

@Component
public class StringToDocumentConverter implements Converter<String, Document> {

    private final DocumentBuilderFactory documentBuilderFactory;

    @Autowired
    public StringToDocumentConverter(DocumentBuilderFactory documentBuilderFactory,
                                     TransformerFactory transformerFactory) {
        this.documentBuilderFactory = documentBuilderFactory;
    }

    @Override
    public Document convert(String source) {
        try {
            return documentBuilderFactory.newDocumentBuilder()
                    .parse(new InputSource(new StringReader(source)));
        } catch (Exception e) {
            throw new RuntimeException("Exception during converting string to Document conversion", e);
        }
    }
}
