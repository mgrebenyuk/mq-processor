package com.kn.mq.processor.service;

import com.kn.mq.processor.model.DocumentData;
import org.w3c.dom.Document;

public interface DocumentService {
    DocumentData process(Document document);
}
