package com.kn.mq.processor.service.impl;

import com.kn.mq.processor.model.DocumentData;
import com.kn.mq.processor.service.DocumentService;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.util.Optional;

@Service
public class DocumentServiceImpl implements DocumentService {

    private final XPathFactory xPathFactory;

    public DocumentServiceImpl(XPathFactory xPathFactory) {
        this.xPathFactory = xPathFactory;
    }

    @Override
    public DocumentData process(Document document) {
        String trnNam = extractTrnNam(document);
        String trnVer = extractTrnVer(document);
        String clientId = extractClientId(document);
        String country = extractCountry(document);
        String routeId = extractRouteId(document);

        return new DocumentData()
                .setTransactionName(trnNam)
                .setTransactionVersion(trnVer)
                .setClientId(clientId)
                .setCountry(country)
                .setRouteId(routeId);
    }

    private String extractTrnNam(Document document) {
        String value = extractValue(document, "/UC_STOCK_LEVEL_IFD/CTRL_SEG/TRNNAM/text()", XPathConstants.STRING);
        return Optional
                .ofNullable(value)
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .orElseThrow(() -> new IllegalArgumentException("/UC_STOCK_LEVEL_IFD/CTRL_SEG/TRNNAM is required"));
    }

    private String extractTrnVer(Document document) {
        String value = extractValue(document, "/UC_STOCK_LEVEL_IFD/CTRL_SEG/TRNVER/text()", XPathConstants.STRING);
        return Optional
                .ofNullable(value)
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .orElseThrow(() -> new IllegalArgumentException("/UC_STOCK_LEVEL_IFD/CTRL_SEG/TRNVER is required"));
    }

    private String extractClientId(Document document) {
        String value = extractValue(document, "/UC_STOCK_LEVEL_IFD/CTRL_SEG/CLIENT_ID/text()", XPathConstants.STRING);
        return Optional
                .ofNullable(value)
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .orElseThrow(() -> new IllegalArgumentException("/UC_STOCK_LEVEL_IFD/CTRL_SEG/CLIENT_ID is required"));
    }

    private String extractCountry(Document document) {
        String value = extractValue(document, "/UC_STOCK_LEVEL_IFD/CTRL_SEG/ISO_2_CTRY_NAME/text()", XPathConstants.STRING);
        return Optional
                .ofNullable(value)
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .orElseThrow(() -> new IllegalArgumentException("/UC_STOCK_LEVEL_IFD/CTRL_SEG/ISO_2_CTRY_NAME is required"));
    }

    private String extractRouteId(Document document) {
        String value = extractValue(document, "/UC_STOCK_LEVEL_IFD/CTRL_SEG/ROUTE_ID/text()", XPathConstants.STRING);
        return Optional
                .ofNullable(value)
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .orElseThrow(() -> new IllegalArgumentException("/UC_STOCK_LEVEL_IFD/CTRL_SEG/ROUTE_ID is required"));
    }

    private <T> T extractValue(Document document, String xPath, QName type) {
        try {
            return (T) xPathFactory.newXPath()
                    .compile(xPath)
                    .evaluate(document, type);
        } catch (Exception e) {
            throw new RuntimeException("Exception during xpath expression value extracting", e);
        }
    }
}
