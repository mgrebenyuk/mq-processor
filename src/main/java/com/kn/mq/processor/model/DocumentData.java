package com.kn.mq.processor.model;

public class DocumentData {
    private String transactionName;
    private String transactionVersion;
    private String clientId;
    private String country;
    private String routeId;

    public String getTransactionName() {
        return transactionName;
    }

    public DocumentData setTransactionName(String transactionName) {
        this.transactionName = transactionName;
        return this;
    }

    public String getTransactionVersion() {
        return transactionVersion;
    }

    public DocumentData setTransactionVersion(String transactionVersion) {
        this.transactionVersion = transactionVersion;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public DocumentData setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public DocumentData setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getRouteId() {
        return routeId;
    }

    public DocumentData setRouteId(String routeId) {
        this.routeId = routeId;
        return this;
    }
}
