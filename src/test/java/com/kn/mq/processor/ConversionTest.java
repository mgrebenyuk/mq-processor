package com.kn.mq.processor;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.kn.mq.processor.converter.DocumentToStringConverter;
import com.kn.mq.processor.converter.StringToDocumentConverter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.Document;

import java.net.URL;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConversionTest {

    @Autowired
    StringToDocumentConverter stringToDocumentConverter;

    @Autowired
    DocumentToStringConverter documentToStringConverter;

    @Test
    public void xmlConversionTest() throws Exception {
        URL url = Resources.getResource("message.xml");
        String initial = Resources.toString(url, Charsets.UTF_8);

        Document document = stringToDocumentConverter.convert(initial);
        String converted = documentToStringConverter.convert(document);

        Assert.assertEquals(initial, converted);
    }

}
