package com.kn.mq.processor;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import java.util.concurrent.CountDownLatch;

@Component
public class DestinationMessageListener {

    private Document receivedDocument;
    private CountDownLatch cdl;

    public DestinationMessageListener() {
        cdl = new CountDownLatch(1);
    }

    public Document getReceivedDocument() {
        return receivedDocument;
    }

    public CountDownLatch getCdl() {
        return cdl;
    }

    @JmsListener(destination = "destination", containerFactory = "jmsListenerContainerFactory")
    public void receive(Document document) {
        this.receivedDocument = document;
        cdl.countDown();
    }
}
