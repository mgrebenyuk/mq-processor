package com.kn.mq.processor;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.kn.mq.processor.converter.StringToDocumentConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.Document;

import java.net.URL;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MQIntegrationTest {


    @Autowired
    StringToDocumentConverter stringToDocumentConverter;

    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    DestinationMessageListener destinationMessageListener;

    Document document;

    @Before
    public void init() throws Exception {
        URL url = Resources.getResource("message.xml");
        String initial = Resources.toString(url, Charsets.UTF_8);
        document = stringToDocumentConverter.convert(initial);
    }

    @Test
    public void test() throws InterruptedException {
        jmsTemplate.convertAndSend("source", document);
        destinationMessageListener.getCdl().await(10, TimeUnit.SECONDS);
        Document receivedDocument = destinationMessageListener.getReceivedDocument();
        Assert.assertNotNull(receivedDocument);
    }
}
