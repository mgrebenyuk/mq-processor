package com.kn.mq.processor;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.kn.mq.processor.converter.StringToDocumentConverter;
import com.kn.mq.processor.model.DocumentData;
import com.kn.mq.processor.service.DocumentService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.Document;

import java.net.URL;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProcessingTest {

    @Autowired
    StringToDocumentConverter stringToDocumentConverter;

    @Autowired
    DocumentService documentService;

    Document document;
    Document invalidDocument;

    @Before
    public void init() throws Exception {
        URL url = Resources.getResource("message.xml");
        String initial = Resources.toString(url, Charsets.UTF_8);

        document = stringToDocumentConverter.convert(initial);

        url = Resources.getResource("invalid_message.xml");
        initial = Resources.toString(url, Charsets.UTF_8);

        invalidDocument = stringToDocumentConverter.convert(initial);
    }

    @Test
    public void processingServiceTest() {
        DocumentData result = documentService.process(document);
        Assert.assertEquals("UC_STOCK_LEVEL", result.getTransactionName());
        Assert.assertEquals("20180100", result.getTransactionVersion());
        Assert.assertEquals("CLIE01", result.getClientId());
        Assert.assertEquals("GB", result.getCountry());
        Assert.assertEquals("186", result.getRouteId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidDocumentProcessingServiceTest() {
        DocumentData result = documentService.process(invalidDocument);
    }
}
