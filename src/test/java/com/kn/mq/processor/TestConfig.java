package com.kn.mq.processor;

import org.apache.activemq.broker.BrokerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {

    @Bean
    public BrokerService broker(@Value("${spring.activemq.broker-url}") String url) {
        try {
            BrokerService broker = new BrokerService();
            broker.addConnector(url);
            broker.start();
            return broker;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
