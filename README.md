# MQ Processor

# Build

First of all to build the project execute folowing bash script:  
`build.sh`

# Run

Requirements:  
Runtime app requires external broker to be started.
After the project is built and ActiveMQ broker is setted up run `run.sh` script
